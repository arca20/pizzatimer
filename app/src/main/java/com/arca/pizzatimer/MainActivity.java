package com.arca.pizzatimer;

import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView pizzaTimer;
    SeekBar seekBarTimer;
    Boolean counterIsActive = false;
    Button button;
    CountDownTimer countDownTimer;
    ImageView pizzaImage;

    public void resetTimer(){
        pizzaTimer.setText("5:00");
        seekBarTimer.setProgress(300);
        seekBarTimer.setEnabled(true);
        countDownTimer.cancel();
        counterIsActive = false;
        button.setText("Starten");
        changePizza(0);
    }
    public void buttonClicked(View view){
        if(counterIsActive){
            resetTimer();
        }else{
            counterIsActive = true;
            seekBarTimer.setEnabled(false);
            button.setText("Abbrechen");

            countDownTimer = new CountDownTimer(seekBarTimer.getProgress() * 1000 + 100,1000) {
                @Override
                public void onTick(long l) {
                    updateTimer((int) l  /1000);
                    if(seekBarTimer.getProgress() != 0){
                        changePizza(8 - (int) (l/1000 /(seekBarTimer.getProgress() /8)));
                    }
                }
                @Override
                public void onFinish() {
                    MediaPlayer mplayer = MediaPlayer.create(getApplicationContext(), R.raw.test);
                    mplayer.start();
                    resetTimer();

                }
            }.start();
        }
    }

    public  void updateTimer(int leftSeconds){
        int minutes = leftSeconds /60;
        int seconds = leftSeconds - (minutes *60);
        String secondString = Integer.toString(seconds);
        if(seconds <= 9){
            secondString ="0" + secondString;
        }
        pizzaTimer.setText(Integer.toString(minutes) + ":" + secondString);
    }
    public void changePizza(int id){
        switch (id){
            case 0: pizzaImage.setImageResource(R.drawable.pizza_0);
            break;
            case 1 :pizzaImage.setImageResource(R.drawable.pizza_1);
            break;
            case 2 :pizzaImage.setImageResource(R.drawable.pizza_2);
            break;
            case 3 :pizzaImage.setImageResource(R.drawable.pizza_3);
            break;
            case 4 :pizzaImage.setImageResource(R.drawable.pizza_4);
            break;
            case 5 :pizzaImage.setImageResource(R.drawable.pizza_5);
            break;
            case 6 :pizzaImage.setImageResource(R.drawable.pizza_6);
            break;
            case 7 :pizzaImage.setImageResource(R.drawable.pizza_7);
            break;
            case 8 :pizzaImage.setImageResource(R.drawable.pizza_8);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        seekBarTimer = findViewById(R.id.seekBarTimer);
        pizzaTimer = findViewById(R.id.pizzaTimer);
        button = findViewById(R.id.button);
        pizzaImage = findViewById(R.id.pizzaImage);

        seekBarTimer.setMin(30);
        seekBarTimer.setMax(900);
        seekBarTimer.setProgress(300);

        seekBarTimer.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                updateTimer(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
    }
}
